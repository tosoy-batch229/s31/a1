/*

1. What built-in JS keyword is used to import packages?
    // -> The built-in JavaScript keyword used to import packages in Node.js is `require()`.

2. What Node.js module/package contains a method for server creation?
    // -> The Node.js module/package that contains a method for server creation is `http`.

3. What is the method of the http object responsible for creating a server using Node.js?
    // -> The method of the http object responsible for creating a server using Node.js is `http.createServer()`.

4. Between server and client, which creates a request?
    // -> Client

5. Between server and client, which creates a response?
    // -> Server


6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
    // -> Node.js

7. What is the largest registry for Node packages?
    // -> npm (Node Package Manager) registry.

*/